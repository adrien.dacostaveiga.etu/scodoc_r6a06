# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""

"""
from flask import current_app

from app.scodoc.sco_logos import find_logo


class Action:
    """Base class for all classes describing an action from from config form."""

    def __init__(self, message, parameters):
        self.message = message
        self.parameters = parameters

    @staticmethod
    def build_action(parameters):
        """Check (from parameters) if some action has to be done and
        then return list of action (or else return None)."""
        raise NotImplementedError

    def display(self):
        """return a str describing the action to be done"""
        return self.message.format_map(self.parameters)

    def execute(self):
        """Executes the action"""
        raise NotImplementedError


GLOBAL = "_"


class LogoRename(Action):
    """Action: rename a logo
    dept_id: dept_id or '-'
    logo_id: logo_id (old name)
    new_name: new_name
    """

    def __init__(self, parameters):
        super().__init__(
            f"Renommage du logo {parameters['logo_id']} en {parameters['new_name']}",
            parameters,
        )

    @staticmethod
    def build_action(parameters):
        dept_id = parameters["dept_key"]
        if dept_id == GLOBAL:
            dept_id = None
        parameters["dept_id"] = dept_id
        if parameters["new_name"]:
            logo = find_logo(
                logoname=parameters["new_name"],
                dept_id=parameters["dept_key"],
                strict=True,
            )
            if logo is None:
                return LogoRename(parameters)

    def execute(self):
        from app.scodoc.sco_logos import rename_logo

        current_app.logger.info(self.message)
        rename_logo(
            old_name=self.parameters["logo_id"],
            new_name=self.parameters["new_name"],
            dept_id=self.parameters["dept_id"],
        )


class LogoUpdate(Action):
    """Action: change a logo
    dept_id: dept_id or '_',
    logo_id: logo_id,
    upload: image file replacement
    """

    def __init__(self, parameters):
        super().__init__(
            f"Modification du logo {parameters['logo_id']} pour le département {parameters['dept_id']}",
            parameters,
        )

    @staticmethod
    def build_action(parameters):
        dept_id = parameters["dept_key"]
        if dept_id == GLOBAL:
            dept_id = None
        parameters["dept_id"] = dept_id
        if parameters["upload"] is not None:
            return LogoUpdate(parameters)
        return None

    def execute(self):
        from app.scodoc.sco_logos import write_logo

        current_app.logger.info(self.message)
        write_logo(
            stream=self.parameters["upload"],
            dept_id=self.parameters["dept_id"],
            name=self.parameters["logo_id"],
        )


class LogoDelete(Action):
    """Action: Delete an existing logo
    dept_id: dept_id or '_',
    logo_id: logo_id
    """

    def __init__(self, parameters):
        super().__init__(
            f"Suppression du logo {parameters['logo_id']} pour le département {parameters['dept_id'] or 'tous'}.",
            parameters,
        )

    @staticmethod
    def build_action(parameters):
        parameters["dept_id"] = parameters["dept_key"]
        if parameters["dept_key"] == GLOBAL:
            parameters["dept_id"] = None
        if parameters["do_delete"]:
            return LogoDelete(parameters)
        return None

    def execute(self):
        from app.scodoc.sco_logos import delete_logo

        current_app.logger.info(self.message)
        delete_logo(name=self.parameters["logo_id"], dept_id=self.parameters["dept_id"])


class LogoInsert(Action):
    """Action: add a new logo
    dept_key: dept_id or '_',
    logo_id: logo_id,
    upload: image file replacement
    """

    def __init__(self, parameters):
        super().__init__(
            f"Ajout du logo {parameters['name']} pour le département {parameters['dept_key']} ({parameters['upload'].filename}).",
            parameters,
        )

    @staticmethod
    def build_action(parameters):
        if parameters["dept_key"] == GLOBAL:
            parameters["dept_id"] = None
        if parameters["upload"] and parameters["name"]:
            logo = find_logo(
                logoname=parameters["name"], dept_id=parameters["dept_key"], strict=True
            )
            if logo is None:
                return LogoInsert(parameters)
        return None

    def execute(self):
        from app.scodoc.sco_logos import write_logo

        dept_id = self.parameters["dept_key"]
        if dept_id == GLOBAL:
            dept_id = None
        current_app.logger.info(self.message)
        write_logo(
            stream=self.parameters["upload"],
            name=self.parameters["name"],
            dept_id=dept_id,
        )
