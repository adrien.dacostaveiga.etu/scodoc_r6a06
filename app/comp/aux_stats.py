##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Quelques classes auxiliaires pour les calculs des notes
"""

import numpy as np


class StatsMoyenne:
    """Une moyenne d'un ensemble étudiants sur quelque chose
    (moyenne générale d'un semestre, d'un module, d'un groupe...)
    et les statistiques associées: min, max, moy, effectif
    """

    def __init__(self, vals):
        """Calcul les statistiques.
        Les valeurs NAN ou non numériques sont toujours enlevées.
        Si vals is None, renvoie des zéros (utilisé pour UE bonus)
        """
        try:
            if vals is None or len(vals) == 0 or np.isnan(vals).all():
                self.moy = self.min = self.max = self.size = self.nb_vals = 0
            else:
                self.moy = np.nanmean(vals)
                self.min = np.nanmin(vals)
                self.max = np.nanmax(vals)
                self.size = len(vals)
                self.nb_vals = self.size - np.count_nonzero(np.isnan(vals))
        except (
            TypeError
        ):  # que des NaN dans un array d'objets, ou ce genre de choses exotiques...
            self.moy = self.min = self.max = self.size = self.nb_vals = 0

    def to_dict(self):
        "Tous les attributs dans un dict"
        return {
            "min": self.min,
            "max": self.max,
            "moy": self.moy,
            "size": self.size,
            "nb_vals": self.nb_vals,
        }
