# -*- coding: utf-8 -*-

"""Formulaires génériques
"""

from flask_wtf import FlaskForm
from wtforms import (
    SubmitField,
)

SUBMIT_MARGE = {"style": "margin-bottom: 10px;"}


class SimpleConfirmationForm(FlaskForm):
    "bête dialogue de confirmation"
    submit = SubmitField("OK", render_kw=SUBMIT_MARGE)
    cancel = SubmitField("Annuler", render_kw=SUBMIT_MARGE)
