from flask import g, url_for
from flask_wtf import FlaskForm
from wtforms import FieldList, Form, DecimalField, validators

from app.models import ApcParcours, ApcReferentielCompetences, UniteEns


class _UEParcoursECTSForm(FlaskForm):
    "Formulaire association ECTS par parcours à une UE"
    # construit dynamiquement ci-dessous


def UEParcoursECTSForm(ue: UniteEns) -> FlaskForm:
    "Génère formulaire association ECTS par parcours à une UE"

    class F(_UEParcoursECTSForm):
        pass

    parcours: list[ApcParcours] = ue.formation.referentiel_competence.parcours
    # Initialise un champs de saisie par parcours
    for parcour in parcours:
        ects = ue.get_ects(parcour, only_parcours=True)
        setattr(
            F,
            f"ects_parcour_{parcour.id}",
            DecimalField(
                f"Parcours {parcour.code}",
                validators=[
                    validators.Optional(),
                    validators.NumberRange(min=0, max=30),
                ],
                default=ects,
            ),
        )
    return F()
