##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""ScoDoc 9.3 : Formulaires / jurys BUT
"""


from flask_wtf import FlaskForm
from wtforms import SubmitField


class FormSemestreValidationAutoBUTForm(FlaskForm):
    "simple form de confirmation"
    submit = SubmitField("Lancer le calcul")
    cancel = SubmitField("Annuler")
