"""config nouvelle permission ViewEtudData: donne aux rôles Ens, Secr, Admin

Revision ID: 3fa988ff8970
Revises: b4859c04205f
Create Date: 2024-01-20 13:59:31.491442

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "3fa988ff8970"
down_revision = "b4859c04205f"
branch_labels = None
depends_on = None


def upgrade():
    # Donne la permission ViewEtudData aux rôles Admin, Ens, Secr
    # cette permission est 1<<30
    op.execute(
        "UPDATE role SET permissions = permissions | (1<<30) where role.name = 'Admin';"
    )
    op.execute(
        "UPDATE role SET permissions = permissions | (1<<30) where role.name = 'Ens';"
    )
    op.execute(
        "UPDATE role SET permissions = permissions | (1<<30) where role.name = 'Secr';"
    )


def downgrade():
    # retire la permission ViewEtudData aux rôles Admin, Ens, Secr
    # cette permission est 1<<30
    op.execute(
        "UPDATE role SET permissions = permissions & ~(1<<30) where role.name = 'Admin';"
    )
    op.execute(
        "UPDATE role SET permissions = permissions & ~(1<<30) where role.name = 'Ens';"
    )
    op.execute(
        "UPDATE role SET permissions = permissions & ~(1<<30) where role.name = 'Secr';"
    )
