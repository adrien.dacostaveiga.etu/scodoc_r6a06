"""assoc UE - Niveau

Revision ID: 6002d7d366e5
Revises: af77ca6a89d0
Create Date: 2022-04-26 12:58:32.929910

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6002d7d366e5"
down_revision = "af77ca6a89d0"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "notes_ue", sa.Column("niveau_competence_id", sa.Integer(), nullable=True)
    )
    op.create_foreign_key(
        "notes_ue_niveau_competence_id_fkey",
        "notes_ue",
        "apc_niveau",
        ["niveau_competence_id"],
        ["id"],
    )
    op.create_table(
        "parcours_modules",
        sa.Column("parcours_id", sa.Integer(), nullable=False),
        sa.Column("module_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["module_id"],
            ["notes_modules.id"],
            # nom ajouté manuellement:
            name="parcours_modules_module_id_fkey",
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["parcours_id"],
            ["apc_parcours.id"],
            # nom ajouté manuellement:
            name="parcours_modules_parcours_id_fkey",
        ),
        sa.PrimaryKeyConstraint("parcours_id", "module_id"),
    )
    op.alter_column(
        "apc_modules_acs", "module_id", existing_type=sa.INTEGER(), nullable=False
    )
    op.alter_column(
        "apc_modules_acs", "app_crit_id", existing_type=sa.INTEGER(), nullable=False
    )
    op.drop_constraint(
        "apc_modules_acs_module_id_fkey", "apc_modules_acs", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_modules_acs_module_id_fkey",
        "apc_modules_acs",
        "notes_modules",
        ["module_id"],
        ["id"],
        ondelete="CASCADE",
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(
        "notes_ue_niveau_competence_id_fkey", "notes_ue", type_="foreignkey"
    )
    op.drop_column("notes_ue", "niveau_competence_id")
    op.drop_table("parcours_modules")
    op.drop_constraint(
        "apc_modules_acs_module_id_fkey", "apc_modules_acs", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_modules_acs_module_id_fkey",
        "apc_modules_acs",
        "notes_modules",
        ["module_id"],
        ["id"],
    )
    op.alter_column(
        "apc_modules_acs", "app_crit_id", existing_type=sa.INTEGER(), nullable=True
    )
    op.alter_column(
        "apc_modules_acs", "module_id", existing_type=sa.INTEGER(), nullable=True
    )
    # ### end Alembic commands ###
