# -*- coding: utf-8 -*-

"""Test Logos

Utilisation :
    créer les variables d'environnement: (indiquer les valeurs
    pour le serveur ScoDoc que vous voulez interroger)

    export SCODOC_URL="https://scodoc.xxx.net/"
    export SCODOC_USER="xxx"
    export SCODOC_PASSWD="xxx"
    export CHECK_CERTIFICATE=0 # ou 1 si serveur de production avec certif SSL valide

    (on peut aussi placer ces valeurs dans un fichier .env du répertoire tests/api).

    Lancer :
        pytest tests/api/test_api_formations.py
"""

import requests

from app.scodoc import sco_utils as scu
from tests.api.setup_test_api import API_URL, CHECK_CERTIFICATE, api_headers
from tests.api.tools_test_api import (
    verify_fields,
    FORMATION_EXPORT_FIELDS,
    FORMATION_EXPORT_UE_FIELDS,
    FORMATION_EXPORT_UE_MATIERE_FIELDS,
    FORMATION_EXPORT_UE_MATIERE_MODULE_FIELDS,
    FORMATION_EXPORT_UE_MATIERE_MODULE_COEF_FIELDS,
    MODULE_FIELDS,
    REF_COMP_FIELDS,
)
from tests.api.tools_test_api import FORMATION_FIELDS, MODIMPL_FIELDS


def test_formations_ids(api_headers):
    """
    Route: /formations_ids
    """
    r = requests.get(
        API_URL + "/formations_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    formations_ids = r.json()
    # Une liste non vide d'entiers
    assert isinstance(formations_ids, list)
    assert len(formations_ids) > 0
    assert all(isinstance(x, int) for x in formations_ids)


def test_formations_by_id(api_headers):
    """
    Route: /formation/<int:formation_id>
    """
    id_formation = 1
    r = requests.get(
        f"{API_URL}/formation/{id_formation}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    formation = r.json()
    assert verify_fields(formation, FORMATION_FIELDS) is True
    assert isinstance(formation["dept_id"], int)
    assert isinstance(formation["acronyme"], str)
    assert isinstance(formation["titre_officiel"], str)
    assert isinstance(formation["formation_code"], str)
    assert formation["code_specialite"] is None or isinstance(
        formation["code_specialite"], str
    )
    assert isinstance(formation["id"], int)
    assert isinstance(formation["titre"], str)
    assert isinstance(formation["version"], int)
    assert isinstance(formation["type_parcours"], int)
    assert formation["referentiel_competence_id"] is None or isinstance(
        formation["referentiel_competence_id"], int
    )
    assert isinstance(formation["formation_id"], int)

    assert id_formation == formation["formation_id"]
    assert id_formation == formation["id"]

    r1 = requests.get(
        f"{API_URL}/formation/{formation['formation_id']}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r1.status_code == 200
    formation1 = r1.json()

    assert formation == formation1

    # ERROR
    id_formation_inexistant = 1516476846861656351
    r_error = requests.get(
        f"{API_URL}/formation/{id_formation_inexistant}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r_error.status_code == 404


def test_formation_export(api_headers):
    """
    Route: /formation/<int:formation_id>/export
    """
    r = requests.get(
        API_URL + "/formation/1/export",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    export_formation = r.json()
    assert verify_fields(export_formation, FORMATION_EXPORT_FIELDS) is True
    assert isinstance(export_formation["acronyme"], str)
    assert isinstance(export_formation["titre_officiel"], str)
    assert isinstance(export_formation["formation_code"], str)
    assert export_formation["code_specialite"] is None or isinstance(
        export_formation["code_specialite"], str
    )
    assert isinstance(export_formation["titre"], str)
    assert isinstance(export_formation["version"], int)
    assert isinstance(export_formation["type_parcours"], int)
    assert export_formation["referentiel_competence_id"] is None or isinstance(
        export_formation["referentiel_competence_id"], int
    )
    assert isinstance(export_formation["ue"], list)

    ues = export_formation["ue"]

    for ue in ues:
        assert verify_fields(ue, FORMATION_EXPORT_UE_FIELDS) is True
        assert isinstance(ue["acronyme"], str)
        assert isinstance(ue["numero"], int)
        assert isinstance(ue["titre"], str)
        assert isinstance(ue["type"], int)
        assert isinstance(ue["ue_code"], str)
        assert isinstance(ue["ects"], float)
        assert isinstance(ue["is_external"], bool)
        assert isinstance(ue["code_apogee"], str)
        assert isinstance(ue["coefficient"], float)
        assert isinstance(ue["semestre_idx"], int)
        assert isinstance(ue["color"], str)
        assert isinstance(ue["reference"], int)
        assert isinstance(ue["matiere"], list)

        matieres = ue["matiere"]

        for matiere in matieres:
            assert verify_fields(matiere, FORMATION_EXPORT_UE_MATIERE_FIELDS)
            assert isinstance(matiere["titre"], str)
            assert isinstance(matiere["numero"], int)
            assert isinstance(matiere["module"], list)

            modules = matiere["module"]
            for module in modules:
                assert verify_fields(module, FORMATION_EXPORT_UE_MATIERE_MODULE_FIELDS)
                assert isinstance(module["titre"], str)
                assert isinstance(module["abbrev"], str)
                assert isinstance(module["code"], str)
                assert isinstance(module["heures_cours"], float)
                assert isinstance(module["heures_td"], float)
                assert isinstance(module["heures_tp"], float)
                assert isinstance(module["coefficient"], float)
                assert isinstance(module["ects"], str)
                assert isinstance(module["semestre_id"], int)
                assert isinstance(module["numero"], int)
                assert isinstance(module["code_apogee"], str)
                assert isinstance(module["module_type"], int)
                assert isinstance(module["coefficients"], list)

                coefficients = module["coefficients"]
                for coef in coefficients:
                    assert verify_fields(
                        coef, FORMATION_EXPORT_UE_MATIERE_MODULE_COEF_FIELDS
                    )
                    assert isinstance(coef["ue_reference"], str)
                    assert isinstance(coef["coef"], str)

        # ERROR
        id_formation_inexistant = 1516476846861656351
        r_error = requests.get(
            f"{API_URL}/formation/export/{id_formation_inexistant}",
            headers=api_headers,
            verify=CHECK_CERTIFICATE,
            timeout=scu.SCO_TEST_API_TIMEOUT,
        )
        assert r_error.status_code == 404


def test_formation_export_with_ids(api_headers):
    """
    Route: /formation/<int:formation_id>/export_with_ids
    """
    r = requests.get(
        API_URL + "/formation/1/export_with_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    export_formation = r.json()
    assert verify_fields(export_formation, FORMATION_EXPORT_FIELDS) is True
    assert isinstance(export_formation["id"], int)
    assert isinstance(export_formation["dept_id"], int)
    assert isinstance(export_formation["formation_id"], int)


def test_moduleimpl(api_headers):
    """
    Route: /formation/moduleimpl/<int:moduleimpl_id>
    """
    moduleimpl_id = 1
    r = requests.get(
        f"{API_URL}/moduleimpl/{moduleimpl_id}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    moduleimpl = r.json()
    assert verify_fields(moduleimpl, MODIMPL_FIELDS) is True
    assert isinstance(moduleimpl["id"], int)
    assert isinstance(moduleimpl["responsable_id"], int)
    assert isinstance(moduleimpl["module_id"], int)
    assert isinstance(moduleimpl["formsemestre_id"], int)
    assert moduleimpl["computation_expr"] is None or isinstance(
        moduleimpl["computation_expr"], str
    )
    assert isinstance(moduleimpl["moduleimpl_id"], int)
    assert isinstance(moduleimpl["ens"], list)
    assert isinstance(moduleimpl["module"], dict)

    module = moduleimpl["module"]
    assert verify_fields(module, MODULE_FIELDS)
    assert isinstance(module["heures_cours"], float)
    assert isinstance(module["semestre_id"], int)
    assert isinstance(module["heures_td"], float)
    assert isinstance(module["numero"], int)
    assert isinstance(module["heures_tp"], float)
    assert isinstance(module["code_apogee"], str)
    assert isinstance(module["titre"], str)
    assert isinstance(module["coefficient"], float)
    assert isinstance(module["module_type"], int)
    assert isinstance(module["id"], int)
    assert module["ects"] is None or isinstance(module["ects"], str)
    assert isinstance(module["abbrev"], str)
    assert isinstance(module["ue_id"], int)
    assert isinstance(module["code"], str)
    assert isinstance(module["formation_id"], int)
    assert isinstance(module["matiere_id"], int)
    assert isinstance(module["module_id"], int)

    assert moduleimpl_id == moduleimpl["id"]
    assert moduleimpl_id == moduleimpl["moduleimpl_id"]

    r1 = requests.get(
        f"{API_URL}/moduleimpl/{moduleimpl['moduleimpl_id']}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r1.status_code == 200
    moduleimpl1 = r1.json()

    assert moduleimpl == moduleimpl1

    # ERROR
    id_formation_inexistant = 1516476846861656351
    r_error = requests.get(
        f"{API_URL}/formation/moduleimpl/{id_formation_inexistant}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r_error.status_code == 404


def test_referentiel_competences(api_headers):
    """
    Route: "/formation/<int:formation_id>/referentiel_competences",
    """
    r = requests.get(
        f"{API_URL}/formation/1/referentiel_competences",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200

    ref_comp = r.json()
    assert verify_fields(ref_comp, REF_COMP_FIELDS) is True
    assert isinstance(ref_comp["dept_id"], int)
    assert isinstance(ref_comp["annexe"], str)
    assert isinstance(ref_comp["specialite"], str)
    assert isinstance(ref_comp["specialite_long"], str)
    assert isinstance(ref_comp["type_structure"], str)
    assert isinstance(ref_comp["type_departement"], str)
    assert isinstance(ref_comp["type_titre"], str)
    assert isinstance(ref_comp["version_orebut"], str)
    assert isinstance(ref_comp["scodoc_date_loaded"], str)
    assert isinstance(ref_comp["scodoc_orig_filename"], str)
    assert isinstance(ref_comp["competences"], dict)
    assert isinstance(ref_comp["parcours"], dict)

    # ERROR
    id_formation_inexistant = 1516476846861656351
    r_error = requests.get(
        f"{API_URL}/formation/{id_formation_inexistant}/referentiel_competences",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r_error.status_code == 404
