"""
Quelques fonctions d'initialisation pour tests unitaires
"""
import datetime

from app import db, models
from app.models import Formation
import app.scodoc.sco_utils as scu
from app.scodoc import codes_cursus
from tests.unit import sco_fake_gen

# Valeurs des notes saisies par les tests:
NOTES_T = [
    float(x) for x in (20, 0, 10, 13 / 7.0, 12.5, 24.0 / 11) + tuple(range(1, 19))
]


def build_formation_test(
    nb_mods=1,
    parcours=codes_cursus.CursusBUT,
    with_ue_sport=False,
    acronyme="F3",
    titre="Formation 2",
) -> tuple[sco_fake_gen.ScoFake, int, list[int], list[int]]:
    """Crée une formation simple pour les tests.
    Création à partir de zéro (n'importe pas un fichier xml).
    Avec 3 UEs en S2 et une UE en S4.
    """
    G = sco_fake_gen.ScoFake(verbose=False)
    # If already exists, just use it
    formation = Formation.query.filter_by(
        dept_id=G.dept.id,
        acronyme=acronyme,
        titre=titre,
        version=1,
    ).first()
    if formation is not None:
        return (
            G,
            formation.id,
            [ue.id for ue in formation.ues],
            [m.id for m in formation.modules],
        )
    formation_id = G.create_formation(
        acronyme=acronyme,
        titre=titre,
        titre_officiel="Titre officiel 2",
        type_parcours=parcours.TYPE_CURSUS,
    )
    _ue1 = G.create_ue(
        formation_id=formation_id,
        acronyme="UE1",
        titre="ue 1",
        semestre_idx=2,
        numero=1,
    )
    _ue2 = G.create_ue(
        formation_id=formation_id,
        acronyme="UE2",
        titre="ue 2",
        semestre_idx=2,
        numero=2,
    )
    _ue3 = G.create_ue(
        formation_id=formation_id,
        acronyme="UE3",
        titre="ue 3",
        semestre_idx=2,
        numero=3,
    )
    ue_ids = [_ue1, _ue2, _ue3]
    # une 4eme UE en dehors du semestre 2
    _ = G.create_ue(
        formation_id=formation_id, acronyme="UE41", titre="ue 41", semestre_idx=4
    )
    matiere_id = G.create_matiere(ue_id=_ue1, titre="matière test")
    module_ids = []
    for i in range(nb_mods):
        module_id = G.create_module(
            matiere_id=matiere_id,
            code=f"TSM{i}",
            coefficient=1.0,
            titre=f"module test {i}",
            semestre_id=2,
            module_type=scu.ModuleType.RESSOURCE
            if parcours.APC_SAE
            else scu.ModuleType.STANDARD,
        )
        module_ids.append(module_id)
    if with_ue_sport:
        ue_sport = G.create_ue(
            formation_id=formation_id,
            acronyme="UESPO",
            titre="ue sport",
            semestre_idx=2,
            type=codes_cursus.UE_SPORT,
        )
        ue_ids.append(ue_sport)
        mat_sport = G.create_matiere(ue_id=ue_sport, titre="matière spo")
        mod_sport = G.create_module(
            matiere_id=mat_sport,
            code="BAD",
            coefficient=1.0,
            titre="module sport",
            semestre_id=2,
        )
        module_ids.append(mod_sport)
    return G, formation_id, ue_ids, module_ids


def build_modules_with_evaluations(
    ue_coefs=(1.0, 2.0, 3.0), nb_mods=1, nb_evals_per_modimpl=1
):
    """Utilisé dans plusieurs tests:
    - création formation 3 UE, 1 module
    - 1 semestre, nb_mod moduleimpl, 1 eval/moduleimpl
    """
    nb_ues = len(ue_coefs)  # 3 UEs par défaut dans ce test
    G, formation_id, (ue1_id, ue2_id, ue3_id), module_ids = build_formation_test(
        nb_mods=nb_mods
    )
    ue1 = db.session.get(models.UniteEns, ue1_id)
    ue2 = db.session.get(models.UniteEns, ue2_id)
    ue3 = db.session.get(models.UniteEns, ue3_id)
    for module_id in module_ids:
        mod = db.session.get(models.Module, module_id)
        # Coef du module vers les UE
        c1, c2, c3 = ue_coefs
        coefs_mod = {ue1.id: c1, ue2.id: c2, ue3.id: c3}
        mod.set_ue_coef_dict(coefs_mod)
        assert mod.get_ue_coef_dict() == coefs_mod
    # Mise en place:
    formsemestre_id = G.create_formsemestre(
        formation_id=formation_id,
        semestre_id=2,
        date_debut="01/01/2021",
        date_fin="30/06/2021",
    )
    evaluation_ids = []
    for module_id in module_ids:
        moduleimpl_id = G.create_moduleimpl(
            module_id=module_id,
            formsemestre_id=formsemestre_id,
        )
        modimpl = db.session.get(models.ModuleImpl, moduleimpl_id)
        assert modimpl.formsemestre.formation.get_cursus().APC_SAE  # BUT
        # Check ModuleImpl
        ues = modimpl.formsemestre.get_ues()
        assert len(ues) == 3
        #
        for _ in range(nb_evals_per_modimpl):
            e = G.create_evaluation(
                moduleimpl_id=moduleimpl_id,
                date_debut=datetime.datetime(2021, 1, 1),
                description="evaluation 1",
                coefficient=0,
            )
            evaluation_ids.append(e["evaluation_id"])
    return G, formation_id, formsemestre_id, evaluation_ids, ue1, ue2, ue3
